# BoxFileSync
Syncronize files and folders with Box cloud
# Requirements
- Python 3.11 or superior
- Box Account
# Installation
```bash
pip install BoxFileSync
```

# Usage
```bash
python-m BoxFileSync
```
# Licence
Licensed under the MIT Licence. More Information in the LICENCE file in this repository
