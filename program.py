from flask import Flask, request, redirect, session
from boxsdk import OAuth2, Client
import boxsdk

app = Flask(__name__)
app.secret_key = 'your secret key'

CLIENT_ID = 'your client id'
CLIENT_SECRET = 'your client secret'

oauth = boxsdk.OAuth2(
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET
)

@app.route('/')
def index():
    auth_url, csrf_token = oauth.get_authorization_url('http://localhost:5000/callback')
    session['csrf_token'] = csrf_token
    return redirect(auth_url)

@app.route('/callback')
def callback():
    csrf_token = session['csrf_token']
    code = request.args.get('code')

    assert request.args['state'] == csrf_token

    access_token, refresh_token = oauth.authenticate(code)


import os
import obj_encrypt
from flask import session

# Using the environment's "SECRET_KEY"
key = os.environ.get("SECRET_KEY")
cipher = obj_encrypt.StringCipher(key)

try:
    # If accessToken or refreshToken are invalid, a ValueError is thrown
    secure_access_token = cipher.decrypt(access_token)
    secure_refresh_token = cipher.decrypt(refresh_token)

    session['access_token'] = secure_access_token
    session['refresh_token'] = secure_refresh_token

except ValueError:
    # Clear the session if the token is invalid
    session.clear()

print("You have successfully authenticated with Box!")

if __name__ == '__main__':
    app.run(debug=True)

client = Client(auth)

file_paths = input("Enter the file's path to sync (separated by comma if multiple): ").split(', ')

folder_name = input("Enter the name of the Box folder to sync files with: ")

items = client.folder(folder_id='0').get_items()
matching_folders = [item for item in items if item.type == 'folder' and item.name == folder_name]

if matching_folders:
    selected_folder = matching_folders[0]
    folder_id = selected_folder.id
    new_files = [selected_folder.upload(file_path) for file_path in file_paths]
    for new_file in new_files:
        print(f'File uploaded to folder: {selected_folder.name}. File ID: {new_file.id}')
else:
    print(f'Folder with name "{folder_name}" not found.')
    sys.exit()